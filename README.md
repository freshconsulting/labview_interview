# LabVIEW_Interview

Clone this repo and create a branch with your name and the date of the interviw.  This will be how you submit your work.  When you are done push your work to that branch.  


###LabVIEW Task

## Serial Number Generator
-	Reading configuration settings
-	Implementing basic VI functionality to generate a serial number based on an algorithm
-	Recording results  

## Create a LabVIEW project with a Main VI that performs the following functionality: 

-	Reads the following configuration settings from an INI file, “config.ini”, that resides in the same folder as the application. The settings section is called [Settings] and contains the following keys:
*	Case-Sensitive : True/False
*	Seed Value: 0x1A
*	The ini file will be read once at the start of the program

##	UI:
*	Has an output field “Serial Number” for the generated serial number
*	Has an input field “Computer Name” for the computer name
*	Has an input field “User” or a user name
*	If the input field “Computer Name” is empty or contains a space, the program will read the computer’s Windows host name from the system and use that as “computer name” instead of the input field value. If that is the case, the program will automatically fill the field “Computer Name” with the host name.
 -The program can be stopped by closing the window (clicking the X) if the user also confirms they want to end the program

##	The algorithm for generating the serial number is as follows: 
* Correctly parse the seed value to an integer in LabVIEW
*	Use the first 6 characters of the “computer name” for the next steps. If it has less than 6 characters, display an error message and ask the user to input a longer name
*	If the “Case-Sensitive” option is false, convert “Computer Name” string to uppercase.
*	Add the seed value to each byte value of the Computer Name ASCII string
*	Add dashes to the resulting serial number to reflect this format: XX-XX-XX

-	Save the following information to a CSV file “log.csv” in the same folder as the program. Append new data to it: 
*	Date
*	Time
*	User Name
*	Computer Name
*	Generate Serial Number
